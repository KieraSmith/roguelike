﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public string playGameLevel;
	public int secondsUntilLevelStart = 2;

	public void PlayGame() 
    {
		Application.LoadLevel (playGameLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}


	public void QuitGame()
	{
		Application.Quit ();
	}

}
